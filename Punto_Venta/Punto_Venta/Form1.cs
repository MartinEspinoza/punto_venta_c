﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Punto_Venta
{
    public partial class Form1 : Form
    {
        //variable de clase(en el mismo nivel del metodo)
        private string[,] productos =
        {
            {"1","Burro percheron","100"},
            {"2","Hot dogs de la uni","60"},
            {"3","Ensalada","130"},
            {"4","Enchiladas","80"},
            {"5","Torta de civil","100"}
        };

        public Form1()
        {

            InitializeComponent(); 
        }

        private void Form1_Load(object sender, EventArgs e)
        {   
            label1.Location = new Point((this.Width / 2) - (label1.Width / 2), 0);
            label2.Text = DateTime.Now.ToLongTimeString() + DateTime.Now.ToLongDateString();
            label2.Location = new Point((this.Width / 2) - (label2.Width / 2), label1.Height + 1);
            dataGridView1.Width = this.Width - 10;
            dataGridView1.Height = this.Height * 3 / 4;
            dataGridView1.Location = new Point(5, label1.Height + label2.Height + 1);
            //textBox1 = this.Width - 10;
            textBox1.Location = new Point(0, this.Height - textBox1.Height - 3);
            textBox1.Width = this.Width;
            label3.Location = new Point(this.Width-dataGridView1.Columns[3].Width,label1.Height + dataGridView1.Height +30);
            button1.Location = new Point(this.Width - dataGridView1.Columns[3].Width-label3.Width -3, label1.Height + dataGridView1.Height + 30);

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label2.Text = DateTime.Now.ToLongTimeString() + DateTime.Now.ToLongDateString();
        }

        private void total()
        {
            float total = 0.0f;
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                total += float.Parse(dataGridView1[3,i].Value.ToString());
            }
            label3.Text = "Total = " + total;
            textBox1.Clear();
            textBox1.Focus();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void buscarProductos()
        {
            if (textBox1.Text.IndexOf('*') != -1)
            {
                String[] ej = textBox1.Text.Split('*');
                for (int i = 0; i < 5; i++)
                {
                    if (ej[1] == productos[i, 0])
                    {
                        dataGridView1.Rows.Add(productos[i,2], productos[i, 1], ej[0], float.Parse(productos[i,2])*float.Parse(ej[0]));
                        total();
                    }
                }
            } else
            {
                for (int i = 0; i < 5; i++)
                {
                    if (textBox1.Text == productos[i, 0])
                    {
                        dataGridView1.Rows.Add(productos[i,2], productos[i, 1], "1");
                        total(); 
                    }
                }
            }
            //MessageBox.Show(dataGridView1[0,0].Value.ToString());
            
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            Form1_Load(sender, e);

        }

        private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

     

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.Enter)
            {
                buscarProductos();
            }
            if (e.KeyCode == Keys.Escape)
            {
                dataGridView1.Rows.RemoveAt(dataGridView1.Rows.Count-1);
                total();
            }
            if (e.KeyCode == Keys.P)
            {
                MessageBox.Show("¿vas a pagar?");
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
